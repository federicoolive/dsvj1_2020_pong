#include "juego.h"

namespace pong
{
    namespace juego
    {
        bool continuar = true;

        void juego()
        {
            InitWindow(screenWidth, screenHeight, "Pong");
            SetExitKey(NULL);

            IniciarBotones();
            IniciarObjetos();

            SetTargetFPS(60);
            //sonidos::init();
            while (!WindowShouldClose() && continuar)
            {
                escenario.tiempo[0] = clock();

                if (IsMouseButtonUp(MOUSE_LEFT_BUTTON))
                {
                    boton.pulsador = true;
                }
                if (IsKeyPressed(teclaSalir) && menuActual[menuInmersion] != MENUACTUAL::SALIR)
                {
                    menuInmersion++;
                    menuActual[menuInmersion] = MENUACTUAL::SALIR;
                }

                //sonidos::update();

                switch (menuActual[menuInmersion])
                {
                case MENUACTUAL::MENUPRINCIPAL:

                    principal::update();

                    break;
                case MENUACTUAL::VERSUS:

                    update();

                    break;
                case MENUACTUAL::CONFIGURACIONES:

                    opciones::update();

                    break;
                case MENUACTUAL::GAMEOVER:

                    gameOver::update();

                    break;
                case MENUACTUAL::SALIR:

                    salir::update();

                    break;
                default:
                    break;
                }
            }
            CloseWindow();
            //sonidos::deinit();
        }

        
    }
}