#ifndef JUEGO_H
#define JUEGO_H

#include "gameplay/pong_versus.h"
using namespace pong::versus;

namespace pong
{
	namespace juego
	{
		const int screenWidth = 800;
		const int screenHeight = 450;

		extern bool continuar;

		void juego();
	}
}

#endif