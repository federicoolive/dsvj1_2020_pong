#include "menues.h"
#include "gameplay/juego.h"

using namespace pong::objetos;

namespace pong
{
    namespace menu
    {
        MENUACTUAL menuActual[(int)MENUACTUAL::SALIR];
        int menuInmersion;
        TECLA teclaPresionada;
        bool esperandoInput = false;

        namespace principal
        {
            void principal::update()
            {
                input();
                dibujar();
            }

            void input()
            {
                if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && boton.pulsador)
                {
                    boton.mouse.x = GetMouseX();
                    boton.mouse.y = GetMouseY();

                    // L�gica de Mouse - Botones
                    if (CheckCollisionPointRec(boton.mouse, boton.menuPrincipalJugar))
                    {
                        menuInmersion++;
                        menuActual[menuInmersion] = MENUACTUAL::VERSUS;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.menuPrincipalConfig))
                    {
                        menuInmersion++;
                        menuActual[menuInmersion] = MENUACTUAL::CONFIGURACIONES;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.salir))
                    {
                        menuInmersion++;
                        menuActual[menuInmersion] = MENUACTUAL::SALIR;
                    }

                    boton.pulsador = false;
                }

            }
            void dibujar()
            {
                BeginDrawing();
                ClearBackground(RAYWHITE);

                DrawRectangleRec(boton.menuPrincipalJugar, BLUE);
                DrawRectangleRec(boton.menuPrincipalConfig, PINK);
                DrawRectangleRec(boton.salir, RED);

                DrawText("Jugar", boton.menuPrincipalJugar.x, boton.menuPrincipalJugar.y, boton.menuPrincipalJugar.height, BLACK);
                DrawText("Opciones", boton.menuPrincipalConfig.x, boton.menuPrincipalConfig.y, boton.menuPrincipalConfig.height, BLACK);
                DrawText("Salir", boton.salir.x, boton.salir.y, boton.salir.height, BLACK);

                DrawText("Creado por: Federico Olive",50,420,30,GRAY);
                DrawText("version 1.0", 745, 440, 10, GRAY);

                EndDrawing();
            }
        }

        namespace opciones
        {
            void update()
            {
                init();

                if (esperandoInput)
                {
                    modificacionTeclas();
                }
                else
                {
                    input();
                }

                dibujar();
            }

            void init()
            {
                if (configuraciones::musica)
                {
                    boton.musicaColor = GREEN;
                }
                else
                {
                    boton.musicaColor = PINK;
                }

                if (configuraciones::efectos)
                {
                    boton.efectoColor = GREEN;
                }
                else
                {
                    boton.efectoColor = PINK;
                }
            }
            void modificacionTeclas()
            {
                switch (teclaPresionada)
                {
                case TECLA::P1ARRIBA:
                    SetearTecla(teclaP1Arriba, esperandoInput);
                    break;

                case TECLA::P1ABAJO:
                    SetearTecla(teclaP1Abajo, esperandoInput);
                    break;

                case TECLA::P2ARRIBA:
                    SetearTecla(teclaP2Arriba, esperandoInput);
                    break;

                case TECLA::P2ABAJO:
                    SetearTecla(teclaP2Abajo, esperandoInput);
                    break;

                case TECLA::PAUSA:
                    SetearTecla(teclaPausa, esperandoInput);
                    break;

                default:
                    break;
                }
            }
            void input()
            {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && boton.pulsador)
                {
                    boton.mouse.x = GetMouseX();
                    boton.mouse.y = GetMouseY();
                    if (CheckCollisionPointRec(boton.mouse, boton.menuOpcionesMusica) || CheckCollisionPointRec(boton.mouse, boton.menuOpcionesMusicaOnOff))
                    {
                        configuraciones::musica = !configuraciones::musica;
                        if (configuraciones::musica)
                        {
                            boton.musicaColor = PINK;
                        }
                        else
                        {
                            boton.musicaColor = GREEN;
                        }
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.menuOpcionesEfectos) || CheckCollisionPointRec(boton.mouse, boton.menuOpcionesEfectosOnOff))
                    {
                        configuraciones::efectos = !configuraciones::efectos;
                        if (configuraciones::efectos)
                        {
                            boton.efectoColor = PINK;
                        }
                        else
                        {
                            boton.efectoColor = GREEN;
                        }
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.salir))
                    {
                        menuInmersion++;
                        menuActual[menuInmersion] = MENUACTUAL::SALIR;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.volver))
                    {
                        menuInmersion--;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, { 100,55,200,30 }))
                    {
                        jugador[0].estadoIA = !jugador[0].estadoIA;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, { 500,55,200,30 }))
                    {
                        jugador[1].estadoIA = !jugador[1].estadoIA;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.volverMenuPrincipal) && menuActual[menuInmersion - 1] != MENUACTUAL::MENUPRINCIPAL)
                    {
                        IniciarObjetos();
                        ReiniciarPuntaje();
                        menuInmersion = 0;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.p1ArribaInteractivo))
                    {
                        teclaP1Arriba = 0;
                        teclaPresionada = TECLA::P1ARRIBA;
                        esperandoInput = true;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.p1AbajoInteractivo))
                    {
                        teclaPresionada = TECLA::P1ABAJO;
                        esperandoInput = true;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.p2ArribaInteractivo))
                    {
                        teclaPresionada = TECLA::P2ARRIBA;
                        esperandoInput = true;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.p2AbajoInteractivo))
                    {
                        teclaPresionada = TECLA::P2ABAJO;
                        esperandoInput = true;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.pausaInteractivo))
                    {
                        teclaPresionada = TECLA::PAUSA;
                        esperandoInput = true;
                    }
                    boton.pulsador = false;
                }
            }
            void dibujar()
            {
                BeginDrawing();
                ClearBackground(RAYWHITE);

                DrawRectangleRec(boton.menuOpcionesMusica, BLUE);
                DrawRectangleRec(boton.menuOpcionesMusicaOnOff, boton.musicaColor);
                DrawRectangleRec(boton.menuOpcionesEfectos, BLUE);
                DrawRectangleRec(boton.menuOpcionesEfectosOnOff, boton.efectoColor);

                if (jugador[0].estadoIA)
                {
                    DrawText("-    IA    -", GetScreenWidth() * 1 / 8, GetScreenHeight() * 1 / 8, boton.p1Arriba.height, BLACK);
                }
                else
                {
                    DrawText("-Jugador 1 -", GetScreenWidth() * 1 / 8, GetScreenHeight() * 1 / 8, boton.p1Arriba.height, BLACK);
                }

                if (jugador[1].estadoIA)
                {
                    DrawText("-    IA    -", GetScreenWidth() * 5 / 8, GetScreenHeight() * 1 / 8, boton.p1Arriba.height, BLACK);
                }
                else
                {
                    DrawText("-Jugador 2 -", GetScreenWidth() * 5 / 8, GetScreenHeight() * 1 / 8, boton.p1Arriba.height, BLACK);
                }

                DrawRectangleRec(boton.p1Arriba, BLACK);
                DrawText("Arriba:", boton.p1Arriba.x + 30, boton.p1Arriba.y + 5, boton.p1Arriba.height - 10, WHITE);
                DibujarBoton(boton.p1ArribaInteractivo, teclaP1Arriba, DARKGRAY);

                DrawRectangleRec(boton.p1Abajo, BLACK);
                DrawText("Abajo:", boton.p1Abajo.x + 30, boton.p1Abajo.y + 5, boton.p1Abajo.height - 10, WHITE);
                DibujarBoton(boton.p1AbajoInteractivo, teclaP1Abajo, DARKGRAY);

                DrawRectangleRec(boton.p2Arriba, BLACK);
                DrawText("Arriba:", boton.p2Arriba.x + 30, boton.p2Arriba.y + 5, boton.p2Arriba.height - 10, WHITE);
                DibujarBoton(boton.p2ArribaInteractivo, teclaP2Arriba, DARKGRAY);

                DrawRectangleRec(boton.p2Abajo, BLACK);
                DrawText("Abajo:", boton.p2Abajo.x + 30, boton.p2Abajo.y + 5, boton.p2Abajo.height - 10, WHITE);
                DibujarBoton(boton.p2AbajoInteractivo, teclaP2Abajo, DARKGRAY);

                DrawRectangleRec(boton.pausa, BLACK);
                DrawText("Pausa:", boton.pausa.x + 30, boton.pausa.y + 5, boton.pausa.height - 10, WHITE);
                DibujarBoton(boton.pausaInteractivo, teclaPausa, DARKGRAY);

                DrawRectangleRec(boton.volver, PINK);
                DrawText("Volver", boton.volver.x, boton.volver.y, boton.volver.height, BLACK);

                DrawRectangleRec(boton.salir, RED);
                DrawText("Salir", boton.salir.x, boton.salir.y, boton.salir.height, BLACK);

                if (menuActual[menuInmersion - 1] != MENUACTUAL::MENUPRINCIPAL)
                {
                    DrawRectangleRec(boton.volverMenuPrincipal, BLUE);
                    DrawText("Al Menu Principal", boton.volverMenuPrincipal.x, boton.volverMenuPrincipal.y, boton.volverMenuPrincipal.height, BLACK);
                }

                DrawText("Musica", boton.menuOpcionesMusica.x, boton.menuOpcionesMusica.y, boton.menuOpcionesMusica.height, BLACK);
                if (configuraciones::musica)
                {
                    DrawText("ON", boton.menuOpcionesMusicaOnOff.x + 50, boton.menuOpcionesMusicaOnOff.y + 3, boton.menuOpcionesMusicaOnOff.height, BLACK);
                }
                else
                {
                    DrawText("OFF", boton.menuOpcionesMusicaOnOff.x + 45, boton.menuOpcionesMusicaOnOff.y + 3, boton.menuOpcionesMusicaOnOff.height, BLACK);
                }
                DrawText("Efectos", boton.menuOpcionesEfectos.x, boton.menuOpcionesEfectos.y, boton.menuOpcionesEfectos.height, BLACK);
                if (configuraciones::efectos)
                {
                    DrawText("ON", boton.menuOpcionesEfectosOnOff.x + 50, boton.menuOpcionesEfectosOnOff.y + 3, boton.menuOpcionesEfectosOnOff.height, BLACK);
                }
                else
                {
                    DrawText("OFF", boton.menuOpcionesEfectosOnOff.x + 45, boton.menuOpcionesEfectosOnOff.y + 3, boton.menuOpcionesEfectosOnOff.height, BLACK);
                }
                EndDrawing();
            }
        }

        namespace salir
        {
            void update()
            {
                input();
                dibujar();
            }

            void input()
            {
                if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && boton.pulsador)
                {
                    boton.mouse.x = GetMouseX();
                    boton.mouse.y = GetMouseY();

                    if (CheckCollisionPointRec(boton.mouse, boton.salir))
                    {
                        pong::juego::continuar = false;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.volver))
                    {
                        menuInmersion--;
                    }
                    boton.pulsador = false;
                }
            }
            void dibujar()
            {
                BeginDrawing();
                ClearBackground(RAYWHITE);

                DrawText("Seguro desea salir?", 150, 180, 50, BLACK);
                DrawRectangleRec(boton.volver, PINK);
                DrawText("Volver", boton.volver.x, boton.volver.y, boton.volver.height, BLACK);

                DrawRectangleRec(boton.salir, RED);
                DrawText("Salir", boton.salir.x, boton.salir.y, boton.salir.height, BLACK);

                EndDrawing();
            }
        }

        namespace gameOver
        {
            void update()
            {
                IniciarObjetos();
                input();

                dibujar();
            }

            void input()
            {
                if (IsMouseButtonDown(MOUSE_LEFT_BUTTON) && boton.pulsador)
                {
                    boton.mouse.x = GetMouseX();
                    boton.mouse.y = GetMouseY();

                    if (CheckCollisionPointRec(boton.mouse, boton.menuGameOverReiniciar))
                    {
                        IniciarObjetos();
                        menuInmersion--;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.menuGameOverVolverMenu))
                    {
                        menuInmersion = 0;
                    }
                    else if (CheckCollisionPointRec(boton.mouse, boton.salir))
                    {
                        menuInmersion = 1;
                        menuActual[menuInmersion] = MENUACTUAL::SALIR;
                    }
                    boton.pulsador = false;
                }
            }
            void dibujar()
            {
                BeginDrawing();
                ClearBackground(RAYWHITE);

                DrawRectangleRec(boton.menuGameOverReiniciar, BLUE);
                DrawRectangleRec(boton.menuGameOverVolverMenu, PINK);
                DrawRectangleRec(boton.salir, RED);

                DrawText("Reiniciar", boton.menuGameOverReiniciar.x, boton.menuGameOverReiniciar.y, boton.menuGameOverReiniciar.height, BLACK);
                DrawText("Volver", boton.menuGameOverVolverMenu.x, boton.menuGameOverVolverMenu.y, boton.menuGameOverVolverMenu.height, BLACK);
                DrawText("Salir", boton.salir.x, boton.salir.y, boton.salir.height, BLACK);

                EndDrawing();
            }
        }
    }
}