#ifndef MENUES_H
#define MENUES_H

#include <iostream>
#include "raylib.h"

#include "resources/resources.h"
#include "configurable/teclado_input.h"
#include "objetos/objetos.h"

using namespace pong::inputs;

namespace pong
{
	namespace menu
	{
		enum class MENUACTUAL { MENUPRINCIPAL, VERSUS, CONFIGURACIONES, GAMEOVER, SALIR };
		enum class TECLA { P1ARRIBA, P1ABAJO, P2ARRIBA, P2ABAJO, PAUSA };

		extern int menuInmersion;
		extern MENUACTUAL menuActual[(int)MENUACTUAL::SALIR];
		extern TECLA teclaPresionada;
		extern bool esperandoInput;

		namespace principal
		{
			void update();

			void input();
			void dibujar();
		}
		
		namespace opciones
		{
			void update();

			void init();
			void modificacionTeclas();
			void input();
			void dibujar();
		}

		namespace salir
		{
			void update();

			void input();
			void dibujar();
		}
		
		namespace gameOver
		{
			void update();

			void input();
			void dibujar();
		}
	}
}

#endif