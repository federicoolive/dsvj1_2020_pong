#include "teclado_input.h"
#include "objetos/objetos.h"

namespace pong
{
    namespace inputs
    {
        int teclaP1Arriba = KEY_W;
        int teclaP1Abajo = KEY_S;
        int teclaP2Arriba = KEY_UP;
        int teclaP2Abajo = KEY_DOWN;

        int teclaPausa = KEY_SPACE;
        int teclaSalir = KEY_ESCAPE;

        void SetearTecla(int& teclaApretada, bool& esperandoInput)
        {
            if (IsKeyPressed(KEY_DOWN))
            {
                teclaApretada = KEY_DOWN;
            }
            else if (IsKeyPressed(KEY_UP))
            {
                teclaApretada = KEY_UP;
            }
            else
            {
                teclaApretada = GetKeyPressed();
            }

            if (teclaApretada != 0)
            {
                esperandoInput = false;
            }
        }

        void DibujarBoton(Rectangle boton, int& tecla, Color color)
        {
            DrawRectangleRec(boton, color);
            short posX = 0;

            char textoBoton[10];

            if (tecla == KEY_UP)
            {
                textoBoton[0] = 'A';
                textoBoton[1] = 'R';
                textoBoton[2] = 'R';
                textoBoton[3] = 'I';
                textoBoton[4] = 'B';
                textoBoton[5] = 'A';
                textoBoton[6] = '\0';
                posX = boton.x + 15;
            }
            else if (tecla == KEY_DOWN)
            {
                textoBoton[0] = 'A';
                textoBoton[1] = 'B';
                textoBoton[2] = 'A';
                textoBoton[3] = 'J';
                textoBoton[4] = 'O';
                textoBoton[5] = '\0';
                posX = boton.x + 20;
            }
            else if (tecla == KEY_SPACE)
            {
                textoBoton[0] = 'E';
                textoBoton[1] = 'S';
                textoBoton[2] = 'P';
                textoBoton[3] = 'A';
                textoBoton[4] = 'C';
                textoBoton[5] = 'I';
                textoBoton[6] = '0';
                textoBoton[7] = '\0';
                posX = boton.x;
            }
            else
            {
                for (int i = 0; i < 10; i++)
                {
                    textoBoton[i] = '\0';
                }
                if (tecla >= 97 && tecla <= 122)
                {
                    tecla -= 32;
                }
                textoBoton[0] = char(tecla);
                posX = boton.x + boton.width / 2 - boton.height / 2 + 7;
            }
            DrawText(textoBoton, posX, boton.y + 4, boton.height, WHITE);
        }
    }

    namespace configuraciones
    {
        int tiempoReinicio = 3000; // tiempo de espera para empezar despues de un gol
        int eventosEspera = 2000;
        int eventosDuracion = 3000;
    }
}