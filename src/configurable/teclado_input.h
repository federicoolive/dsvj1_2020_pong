#ifndef TECLADO_INPUT_H
#define TECLADO_INPUT_H

#include <iostream>
#include <string>

#include "raylib.h"

namespace pong
{
	namespace inputs
	{
		extern int teclaP1Arriba;
		extern int teclaP1Abajo;
		extern int teclaP2Arriba;
		extern int teclaP2Abajo;

		extern int teclaPausa;
		extern int teclaSalir;

		void SetearTecla(int& teclaApretada, bool& esperandoInput);
		void DibujarBoton(Rectangle boton, int& tecla, Color color);
	}

	namespace configuraciones
	{
		extern int tiempoReinicio;
		extern int eventosEspera;
		extern int eventosDuracion;
	}
}

#endif