#include "evento_random.h"
#include "objetos/objetos.h"

namespace pong
{
	namespace eventos
	{
		int tiempoEvento[2];
		bool obstaculoActivado = false;
		bool bajarObstaculo = true;
		EVENTORANDOM eventoRandom;
		Rectangle obstaculo;
		int velocidadObjetos = 10;
		bool rebote = false;
		void activarEventoRandom()
		{
			tiempoEvento[0] = clock();
			switch (eventoRandom.eventoActual)
			{
			case EVENTOS::SINEVENTO:
				eventoRandom.eventoActivado = false;
				if (tiempoEvento[1] + pong::configuraciones::eventosEspera < tiempoEvento[0])
				{
					eventoRandom.eventoActual = (EVENTOS)(rand() % ((int)EVENTOS::LIMITE - 1) + 1);
				}

				break;
			case EVENTOS::OBSTACULOS:

				eventoObstaculo();

				break;
			case EVENTOS::INVERTIRVEL:

				eventoInvertirVelocidad();

				break;
			case EVENTOS::INVERTIRCONTROLES:

				eventoInvertirControles();

				break;
			case EVENTOS::MULTIBALL:

				eventoMultiBall();

				break;
			default:
				eventoRandom.eventoActual = EVENTOS::SINEVENTO;
				break;
			}
		}

		void eventoObstaculo()
		{
			if (!eventoRandom.eventoActivado)
			{
				std::cout << "Init Obstaculos\n";
				initObstaculos();

				tiempoEvento[1] = clock();
				eventoRandom.eventoActivado = true;
			}
			else if (tiempoEvento[1] + eventoRandom.duracion < tiempoEvento[0])
			{
				std::cout << "Fin\n";
				tiempoEvento[1] = clock();
				deInitObstaculos();
				eventoRandom.eventoActual = EVENTOS::SINEVENTO;
			}
			else
			{
				if (bajarObstaculo)
				{
					obstaculo.y += velocidadObjetos;
					if (obstaculo.y + obstaculo.height > GetScreenHeight())
					{
						bajarObstaculo = false;
					}
				}
				else
				{
					obstaculo.y -= velocidadObjetos;
					if (obstaculo.y < 0)
					{
						bajarObstaculo = true;
					}
				}
			}
		}

		void dibujarObstaculo()
		{
			if (obstaculoActivado)
			{
				DrawRectangleRec(obstaculo, BLACK);
			}
		}

		void eventoInvertirVelocidad()
		{
			if (!eventoRandom.eventoActivado)
			{
				std::cout << "Init InvertirVelocidad\n";
				pong::objetos::pelota[0].vel.x *= -1;
				pong::objetos::pelota[0].vel.y *= -1;

				tiempoEvento[1] = clock();
				eventoRandom.eventoActivado = true;
			}
			else if (tiempoEvento[1] + eventoRandom.duracion < tiempoEvento[0])
			{
				std::cout << "Fin\n";
				tiempoEvento[1] = clock();
				eventoRandom.eventoActual = EVENTOS::SINEVENTO;
			}
		}

		void eventoInvertirControles()
		{
			if (!eventoRandom.eventoActivado)
			{
				std::cout << "Init Invertir Controles\n";
				int teclaAux = teclaP1Abajo;
				teclaP1Abajo = teclaP1Arriba;
				teclaP1Arriba = teclaAux;

				teclaAux = teclaP2Abajo;
				teclaP2Abajo = teclaP2Arriba;
				teclaP2Arriba = teclaAux;

				tiempoEvento[1] = clock();
				eventoRandom.eventoActivado = true;
			}
			else if (tiempoEvento[1] + eventoRandom.duracion < tiempoEvento[0])
			{
				std::cout << "Fin\n";
				int teclaAux = teclaP1Abajo;
				teclaP1Abajo = teclaP1Arriba;
				teclaP1Arriba = teclaAux;

				teclaAux = teclaP2Abajo;
				teclaP2Abajo = teclaP2Arriba;
				teclaP2Arriba = teclaAux;

				tiempoEvento[1] = clock();
				eventoRandom.eventoActual = EVENTOS::SINEVENTO;
			}
		}

		void eventoMultiBall()
		{
			if (objetos::pelota[0].centro.x > GetScreenWidth() / 2 - 100 && objetos::pelota[0].centro.x < GetScreenWidth() / 2 + 100)
			{
				if (!eventoRandom.eventoActivado)
				{
					std::cout << "Init Multiball\n";
					objetos::pelota[1] = objetos::pelota[0];
					objetos::pelota[1].vel = { objetos::pelota[0].vel.x * -1,objetos::pelota[0].vel.y * -1 };
					pong::objetos::pelota[1].estado = true;

					tiempoEvento[1] = clock();
					eventoRandom.eventoActivado = true;
				}
				else if (tiempoEvento[1] + pong::configuraciones::eventosDuracion < tiempoEvento[0])
				{
					std::cout << "Fin\n";

					pong::objetos::pelota[1].estado = false;

					tiempoEvento[1] = clock();
					eventoRandom.eventoActual = EVENTOS::SINEVENTO;
				}
			}
		}

		void initObstaculos()
		{
			velocidadObjetos = 5;
			obstaculoActivado = true;
			bajarObstaculo = true;
			obstaculo.width = pong::objetos::jugador[0].pos.width*2;
			obstaculo.height = pong::objetos::jugador[0].pos.height;
			obstaculo.height = 100;
			obstaculo.x = GetScreenWidth() / 2 - obstaculo.width / 2;
			obstaculo.y = 0;
		}
		void deInitObstaculos()
		{
			obstaculoActivado = false;
		}
	}
}