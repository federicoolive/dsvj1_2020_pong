#ifndef EVENTO_RANDOM_H
#define EVENTO_RANDOM_H

#include <stdlib.h>
#include <time.h>

#include "raylib.h"
#include "objetos/objetos.h"

namespace pong
{
	namespace eventos
	{
		enum class EVENTOS { SINEVENTO, OBSTACULOS, INVERTIRVEL, INVERTIRCONTROLES, MULTIBALL, LIMITE };

		struct EVENTORANDOM
		{
			EVENTOS eventoActual;
			int elementoGuardado;
			bool eventoActivado;
			int espera = 2000;
			int duracion = 5000;
			Rectangle obstaculo;
		};

		extern int tiempoEvento[2];
		extern bool obstaculoActivado;
		extern bool bajarObstaculo;
		extern EVENTORANDOM eventoRandom;
		extern Rectangle obstaculo;
		extern int velocidadObjetos;
		extern bool rebote;
		void activarEventoRandom();
		void eventoObstaculo();
		void dibujarObstaculo();
		void eventoInvertirVelocidad();
		void eventoInvertirControles();
		void eventoMultiBall();
		void initObstaculos();
		void deInitObstaculos();
	}
}

#endif