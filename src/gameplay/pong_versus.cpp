#include "gameplay/pong_versus.h"

namespace pong
{
    namespace versus
    {
        void update()
        {
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && boton.pulsador)
            {
                boton.mouse.x = GetMouseX();
                boton.mouse.y = GetMouseY();
                //std::cout << boton.mouse.x << " |||| " << boton.mouse.y << "\n";
            }

            if (!escenario.enjuego)
            {
                if (jugador[0].puntaje == 0 && jugador[1].puntaje == 0)
                {
                    seleccionDeColor();
                }
                
                if (IsKeyDown(teclaPausa) && escenario.tiempo[0] - escenario.tiempo[1] > 250)
                {
                    escenario.tiempo[1] = escenario.tiempo[0];

                    escenario.enjuego = true;
                    tiempoPowerUp[1] = clock();
                    pong::eventos::tiempoEvento[1] = clock();
                }
            }
            else
            {
                if (powerUp.turnoPowerUp == 1)
                {
                    activarPowerUpJugador01();
                }
                else if (powerUp.turnoPowerUp == 2)
                {
                    activarPowerUpJugador02();
                }

                eventos::activarEventoRandom();

                if (!jugador[0].estadoIA)
                {
                    movimientoDeJugador01();
                }
                else
                {
                    comportamientoIA01();
                }

                if (!jugador[1].estadoIA)
                {
                    movimientoDeJugador02();
                }
                else
                {
                    comportamientoIA02();
                }

                if (IsKeyDown(teclaPausa) && escenario.tiempo[0] - escenario.tiempo[1] > 250)
                {
                    menuInmersion++;
                    menuActual[menuInmersion] = MENUACTUAL::CONFIGURACIONES;
                }

                movimientoDePelota();

                chequeoDeColisiones();
            }

            dibujar();
        }

        void seleccionDeColor()
        {
            if (IsKeyDown(teclaP1Arriba) && jugador[0].pos.y > 0 && escenario.tiempo[0] - escenario.tiempo[1] > 250)
            {
                escenario.tiempo[1] = escenario.tiempo[0];
                if (jugador[0].colorActual > 0)
                {
                    jugador[0].colorActual--;
                }
            }
            if (IsKeyDown(teclaP1Abajo) && jugador[0].pos.y + jugador[0].pos.height < GetScreenHeight() && escenario.tiempo[0] - escenario.tiempo[1] > 250)
            {
                escenario.tiempo[1] = escenario.tiempo[0];
                if (jugador[0].colorActual < 9)
                {
                    jugador[0].colorActual++;
                }
            }
            if (IsKeyDown(teclaP2Arriba) && jugador[1].pos.y > 0 && escenario.tiempo[0] - escenario.tiempo[1] > 250)
            {
                escenario.tiempo[1] = escenario.tiempo[0];
                if (jugador[1].colorActual > 0)
                {
                    jugador[1].colorActual--;
                }
            }
            if (IsKeyDown(teclaP2Abajo) && jugador[1].pos.y + jugador[1].pos.height < GetScreenHeight() && escenario.tiempo[0] - escenario.tiempo[1] > 250)
            {
                escenario.tiempo[1] = escenario.tiempo[0];
                if (jugador[1].colorActual < 9)
                {
                    jugador[1].colorActual++;
                }
            }
        }

        void movimientoDeJugador01()
        {
            if (IsKeyDown(teclaP1Arriba) && jugador[0].pos.y > 0)
            {
                jugador[0].pos.y -= jugador[0].velocidad;
            }
            if (IsKeyDown(teclaP1Abajo) && jugador[0].pos.y + jugador[0].pos.height < GetScreenHeight())
            {
                jugador[0].pos.y += jugador[0].velocidad;
            }
        }
        void movimientoDeJugador02()
        {
            if (IsKeyDown(teclaP2Arriba) && jugador[1].pos.y > 0)
            {
                jugador[1].pos.y -= jugador[1].velocidad;
            }
            if (IsKeyDown(teclaP2Abajo) && jugador[1].pos.y + jugador[1].pos.height < GetScreenHeight())
            {
                jugador[1].pos.y += jugador[1].velocidad;
            }
        }
        void comportamientoIA01()
        {
            if (pelota[0].vel.x < 0)
            {
                if (jugador[0].pos.y + jugador[0].pos.height / 2 > pelota[0].centro.y && jugador[0].pos.y > 0)
                {
                    jugador[0].pos.y -= jugador[0].velocidad;
                }
                else if (jugador[0].pos.y + jugador[0].pos.height < GetScreenHeight())
                {
                    jugador[0].pos.y += jugador[0].velocidad;
                }
            }
            else
            {

            }
        }
        void comportamientoIA02()
        {
            if (pelota[0].vel.x > 0)
            {
                if (jugador[1].pos.y + jugador[1].pos.height / 2 > pelota[0].centro.y && jugador[1].pos.y > 0)
                {
                    jugador[1].pos.y -= jugador[1].velocidad;
                }
                else if (jugador[1].pos.y + jugador[1].pos.height < GetScreenHeight())
                {
                    jugador[1].pos.y += jugador[1].velocidad;
                }
            }
            else
            {

            }
        }
        void movimientoDePelota()
        {
            escenario.tiempo[2] = clock();
            for (int i = 0; i < pelotasTotales; i++)
            {
                if (pelota[i].estado)
                {
                    if (escenario.tiempo[2] - 4000 > escenario.tiempo[1])
                    {
                        escenario.tiempo[1] = clock();
                        pelota[i].vel.x += (pelota[i].vel.x * 1 / 3);

                        if (pelota[i].aumentoY)
                        {
                            pelota[i].vel.y += (pelota[i].vel.y * 1 / 3);
                        }
                        pelota[i].aumentoY = !pelota[i].aumentoY;
                    }

                    pelota[i].centro.y += pelota[i].vel.y;
                    pelota[i].centro.x += pelota[i].vel.x;
                }
            }
        }

        void chequeoDeColisiones()
        {
            // Pelota Vs Jugador01
            for (int i = 0; i < pelotasTotales; i++)
            {
                if (pelota[i].estado)
                {
                    if (CheckCollisionPointCircle({ jugador[0].pos.x + jugador[0].pos.width, jugador[0].pos.y }, pelota[i].centro, pelota[i].radio) && pelota[i].vel.x < 0)
                    {
                        pelota[i].vel.x *= -1;
                        if (pelota[i].centro.x < jugador[0].pos.x)
                        {
                            pelota[i].vel.y *= -1;
                        }
                        eventos::rebote = true;
                    }
                    else if (CheckCollisionPointCircle({ jugador[0].pos.x + jugador[0].pos.width,jugador[0].pos.y + jugador[0].pos.height }, pelota[i].centro, pelota[i].radio) && pelota[i].vel.x < 0)
                    {
                        pelota[i].vel.x *= -1;
                        if (pelota[i].centro.x > jugador[0].pos.x)
                        {
                            pelota[i].vel.y *= -1;
                        }
                        eventos::rebote = true;
                    }
                    else if (CheckCollisionCircleRec(pelota[i].centro, pelota[i].radio, jugador[0].pos) && pelota[i].vel.x < 0)
                    {
                        pelota[i].vel.x *= -1;
                        eventos::rebote = true;
                    }
                }
            }

            // Pelota Vs Jugador02
            for (int i = 0; i < pelotasTotales; i++)
            {
                if (pelota[i].estado)
                {
                    if (CheckCollisionPointCircle({ jugador[1].pos.x + jugador[1].pos.width,jugador[1].pos.y }, pelota[i].centro, pelota[i].radio) && pelota[i].vel.x > 0)
                    {
                        pelota[i].vel.x *= -1;
                        if (pelota[i].centro.x < jugador[1].pos.x)
                        {
                            pelota[i].vel.y *= -1;
                        }
                        eventos::rebote = true;
                    }
                    else if (CheckCollisionPointCircle({ jugador[1].pos.x + jugador[1].pos.width,jugador[1].pos.y + jugador[1].pos.height }, pelota[i].centro, pelota[i].radio) && pelota[i].vel.x > 0)
                    {
                        pelota[i].vel.x *= -1;
                        if (pelota[i].centro.x > jugador[1].pos.x)
                        {
                            pelota[i].vel.y *= -1;
                        }
                        eventos::rebote = true;
                    }
                    else if (CheckCollisionCircleRec(pelota[i].centro, pelota[i].radio, jugador[1].pos) && pelota[i].vel.x > 0)
                    {
                        pelota[i].vel.x *= -1;
                        eventos::rebote = true;
                    }
                }
            }

            // Pelota Vs Escenario
            for (int i = 0; i < pelotasTotales; i++)
            {
                if (pelota[i].estado)
                {
                    if (pelota[i].centro.y + pelota[i].radio >= GetScreenHeight() && pelota[i].vel.y > 0)    // Inferior
                    {
                        pelota[i].vel.y *= -1;
                    }
                    else if (pelota[i].centro.y - pelota[i].radio <= 0 && pelota[i].vel.y < 0)               // Superior
                    {
                        pelota[i].vel.y *= -1;
                    }

                    if (CheckCollisionCircleRec(pelota[i].centro, pelota[i].radio, gameOver02.pos))       // Derecha
                    {
                        jugador[0].puntaje++;
                        if (jugador[0].puntaje >= escenario.puntuacionMax)
                        {
                            powerUp.turnoPowerUp = 0;
                            ReiniciarPuntaje();
                            menuInmersion++;
                            menuActual[menuInmersion] = MENUACTUAL::GAMEOVER;
                        }
                        else
                        {
                            powerUp.turnoPowerUp = 1;
                            IniciarObjetos();
                            escenario.enjuego = false;
                        }
                        eventos::deInitObstaculos();
                    }
                    else if (CheckCollisionCircleRec(pelota[i].centro, pelota[i].radio, gameOver01.pos))  // Izquierda
                    {
                        jugador[1].puntaje++;
                        if (jugador[1].puntaje >= escenario.puntuacionMax)
                        {
                            powerUp.turnoPowerUp = 0;
                            ReiniciarPuntaje();
                            menuInmersion++;
                            menuActual[menuInmersion] = MENUACTUAL::GAMEOVER;
                        }
                        else
                        {
                            powerUp.turnoPowerUp = 2;
                            IniciarObjetos();
                            escenario.enjuego = false;
                        }
                        eventos::deInitObstaculos();
                    }
                }
            }

            // Pelota Vs Obstaculo
            if (eventos::obstaculoActivado)
            {
                if (CheckCollisionPointCircle({ eventos::obstaculo.x + eventos::obstaculo.width, eventos::obstaculo.y }, pelota[0].centro, pelota[0].radio) && pelota[0].vel.x < 0)
                {
                    pelota[0].vel.x *= -1;
                    if (pelota[0].centro.x < eventos::obstaculo.x)
                    {
                        pelota[0].vel.y *= -1;
                    }
                    eventos::rebote = false;
                    eventos::bajarObstaculo = !eventos::bajarObstaculo;
                }
                else if (CheckCollisionPointCircle({ eventos::obstaculo.x + eventos::obstaculo.width,eventos::obstaculo.y + eventos::obstaculo.height }, pelota[0].centro, pelota[0].radio) && pelota[0].vel.x < 0)
                {
                    pelota[0].vel.x *= -1;
                    if (pelota[0].centro.x > eventos::obstaculo.x)
                    {
                        pelota[0].vel.y *= -1;
                    }
                    eventos::rebote = false;
                    eventos::bajarObstaculo = !eventos::bajarObstaculo;
                }
                else if (CheckCollisionCircleRec(pelota[0].centro, pelota[0].radio, eventos::obstaculo) && pelota[0].vel.x < 0)
                {
                    pelota[0].vel.x *= -1;
                    eventos::rebote = false;
                    eventos::bajarObstaculo = !eventos::bajarObstaculo;
                }
            }
        }
        void dibujar()
        {
            BeginDrawing();

            if (!escenario.enjuego && jugador[0].puntaje == 0 && jugador[1].puntaje == 0)
            {
                Color colorTriangSup01 = GRAY;
                Color colorTriangInf01 = GRAY;
                Color colorTriangSup02 = GRAY;
                Color colorTriangInf02 = GRAY;
                if (jugador[0].colorActual != 0)
                {
                    colorTriangSup01 = jugador[0].color[jugador[0].colorActual - 1];
                }
                if (jugador[0].colorActual != 9)
                {
                    colorTriangInf01 = jugador[0].color[jugador[0].colorActual + 1];
                }
                if (jugador[1].colorActual != 0)
                {
                    colorTriangSup02 = jugador[1].color[jugador[1].colorActual - 1];
                }
                if (jugador[1].colorActual != 9)
                {
                    colorTriangInf02 = jugador[1].color[jugador[1].colorActual + 1];
                }

                DrawTriangle({ jugador[0].pos.x, jugador[0].pos.y - 10 }, { jugador[0].pos.x + jugador[0].pos.width, jugador[0].pos.y - 10 }, { jugador[0].pos.x + jugador[0].pos.width / 2, jugador[0].pos.y - 40 }, colorTriangSup01);
                DrawTriangle({ jugador[0].pos.x, jugador[0].pos.y + jugador[0].pos.height + 10 }, { jugador[0].pos.x + jugador[0].pos.width / 2, jugador[0].pos.y + jugador[0].pos.height + 40 }, { jugador[0].pos.x + jugador[0].pos.width, jugador[0].pos.y + jugador[0].pos.height + 10 }, colorTriangInf01);
                DrawTriangle({ jugador[1].pos.x, jugador[1].pos.y - 10 }, { jugador[1].pos.x + jugador[1].pos.width, jugador[1].pos.y - 10 }, { jugador[1].pos.x + jugador[1].pos.width / 2, jugador[1].pos.y - 40 }, colorTriangSup02);
                DrawTriangle({ jugador[1].pos.x, jugador[1].pos.y + jugador[1].pos.height + 10 }, { jugador[1].pos.x + jugador[1].pos.width / 2, jugador[1].pos.y + jugador[1].pos.height + 40 }, { jugador[1].pos.x + jugador[1].pos.width, jugador[1].pos.y + jugador[1].pos.height + 10 }, colorTriangInf02);
            }

            DrawRectangleRec(gameOver01.pos, gameOver01.color);
            DrawRectangleRec(gameOver02.pos, gameOver02.color);

            DrawRectangleRec(jugador[0].pos, jugador[0].color[jugador[0].colorActual]);
            DrawRectangleRec(jugador[1].pos, jugador[1].color[jugador[1].colorActual]);

            pong::eventos::dibujarObstaculo();

            for (int i = 0; i < pelotasTotales; i++)
            {
                if (pelota[i].estado)
                {
                    DrawCircle(pelota[i].centro.x, pelota[i].centro.y, pelota[i].radio, pelota[i].color);
                }
            }

            DrawText(TextFormat("%c", (48 + jugador[0].puntaje)), 12, 10, 50, BLACK);
            DrawText(TextFormat("%c", (48 + jugador[1].puntaje)), 742, 10, 50, BLACK);

            ClearBackground(RAYWHITE);
            EndDrawing();
        }
    }
}
