#ifndef PONG_VERSUS_H
#define PONG_VERSUS_H

#include <time.h>
#include "raylib.h"

#include "objetos/objetos.h"
#include "menu/menues.h"
#include "configurable/teclado_input.h"
#include "power_up.h"
#include "evento_random.h"

using namespace pong::objetos;
using namespace pong::menu;
using namespace pong::inputs;

namespace pong
{
	namespace versus
	{
		void update();
		void seleccionDeColor();
		void movimientoDeJugador01();
		void movimientoDeJugador02();
		void comportamientoIA01();
		void comportamientoIA02();
		void movimientoDePelota();
		void chequeoDeColisiones();
		void dibujar();
	}
}

#endif