#include "power_up.h"
#include "objetos/objetos.h"

using namespace pong::objetos;

namespace pong
{
	namespace powerups
	{
		int tiempoPowerUp[2];
		POWERUPJUGADORES powerUp;

		void activarPowerUpJugador01()
		{
			tiempoPowerUp[0] = clock();
			switch (powerUp.powerUpActual)
			{
			case POWERUPJUGADOR::NORMAL:
				powerUp.powerUpActivado = false;
				if (tiempoPowerUp[1] + powerUp.espera < tiempoPowerUp[0])
				{
					powerUp.powerUpActual = (POWERUPJUGADOR)(rand() % ((int)POWERUPJUGADOR::LIMITE - 1) + 1);
					std::cout << "Sin Power Up\n";
				}
				break;

			case POWERUPJUGADOR::BARRAVELOZ:

				powerUpBarraVeloz(jugador[0].velocidad);

				break;

			case POWERUPJUGADOR::AGRANDARBARRA:

				powerUpAgrandarBarra(jugador[0].pos.height);

				break;

			case POWERUPJUGADOR::ESCUDO:

				powerUpEscudo(jugador[0].pos.x);

				break;

			default:
				powerUp.powerUpActual = POWERUPJUGADOR::NORMAL;
				break;
			}
		}
		void activarPowerUpJugador02()
		{
			tiempoPowerUp[0] = clock();
			switch (powerUp.powerUpActual)
			{
			case POWERUPJUGADOR::NORMAL:
				powerUp.powerUpActivado = false;
				if (tiempoPowerUp[1] + powerUp.espera < tiempoPowerUp[0])
				{
					powerUp.powerUpActual = (POWERUPJUGADOR)(rand() % ((int)POWERUPJUGADOR::LIMITE - 1) + 1);
				}
				break;

			case POWERUPJUGADOR::BARRAVELOZ:

				powerUpBarraVeloz(jugador[1].velocidad);

				break;

			case POWERUPJUGADOR::AGRANDARBARRA:

				powerUpAgrandarBarra(jugador[1].pos.height);

				break;

			case POWERUPJUGADOR::ESCUDO:

				powerUpEscudo(jugador[1].pos.x);

				break;

			default:
				powerUp.powerUpActual = POWERUPJUGADOR::NORMAL;
				break;
			}
		}

		void powerUpBarraVeloz(int& velocidad)
		{
			if (!powerUp.powerUpActivado)
			{
				powerUp.elementoGuardado = velocidad;
				velocidad *= 2;

				tiempoPowerUp[1] = clock();
				powerUp.powerUpActivado = true;
			}
			else if (tiempoPowerUp[1] + powerUp.duracion < tiempoPowerUp[0])
			{
				velocidad = powerUp.elementoGuardado;
				tiempoPowerUp[1] = clock();
				powerUp.powerUpActual = POWERUPJUGADOR::NORMAL;
			}
		}
		void powerUpAgrandarBarra(float& alto)
		{
			if (!powerUp.powerUpActivado)
			{
				powerUp.elementoGuardado = alto;
				alto *= 2;

				tiempoPowerUp[1] = clock();
				powerUp.powerUpActivado = true;
			}
			else if (tiempoPowerUp[1] + powerUp.duracion < tiempoPowerUp[0])
			{
				alto = powerUp.elementoGuardado;
				tiempoPowerUp[1] = clock();
				powerUp.powerUpActual = POWERUPJUGADOR::NORMAL;
			}
		}
		void powerUpEscudo(float& posX)
		{
			if (!powerUp.powerUpActivado)
			{
				powerUp.escudo.x = posX;

				tiempoPowerUp[1] = clock();
				powerUp.powerUpActivado = true;
			}
			else if (tiempoPowerUp[1] + powerUp.duracion < tiempoPowerUp[0])
			{
				powerUp.escudo.x = 0;

				tiempoPowerUp[1] = clock();
				powerUp.powerUpActual = POWERUPJUGADOR::NORMAL;
			}
		}
	}
}