#ifndef POWER_UP_H
#define POWER_UP_H

#include <time.h>
#include <stdlib.h>

#include "raylib.h"

namespace pong
{
    namespace powerups
    {
        enum class POWERUPJUGADOR { NORMAL, BARRAVELOZ, AGRANDARBARRA, ESCUDO, LIMITE };

        struct POWERUPJUGADORES
        {
            POWERUPJUGADOR powerUpActual = POWERUPJUGADOR::NORMAL;
            int turnoPowerUp = 0;

            int espera = 2000;
            int duracion = 6000;

            int elementoGuardado = 0;
            bool powerUpActivado;

            Rectangle escudo;
        };

        extern int tiempoPowerUp[2];
        extern POWERUPJUGADORES powerUp;

        void activarPowerUpJugador01();
        void activarPowerUpJugador02();

        void powerUpBarraVeloz(int& velocidad);
        void powerUpAgrandarBarra(float& alto);
        void powerUpEscudo(float& posX);
    }
}

#endif