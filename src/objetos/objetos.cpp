#include "objetos.h"

using namespace pong::menu;

namespace pong
{
    namespace objetos
    {
        JUGADOR jugador[jugadoresTotales];
        PELOTA pelota[pelotasTotales];
        GAMEOVER gameOver01;
        GAMEOVER gameOver02;
        BOTONES boton;
        ESCENARIO escenario;
        
        void IniciarObjetos()
        {
            //------------------------------------
            configuraciones::tiempoReinicio = 3000;
            configuraciones::eventosEspera = 2000;
            configuraciones::eventosDuracion = 3000;
            //------------------------------------
            srand(time(NULL));
            //  Parámetros Iniciales
            pong::eventos::rebote = false;
            pong::eventos::eventoRandom.eventoActual = pong::eventos::EVENTOS::SINEVENTO;
            int anchoPantalla = GetScreenWidth();
            int altoPantalla = GetScreenHeight();
            int barrasAncho = 20;
            int barrasAlto = 100;
            int barraDistancia = 50;
            int velocidad = 5;

            // Inicialización del Jugador 1
            jugador[0].radioEsquina = 100;
            jugador[0].pos.width = barrasAncho;
            jugador[0].pos.height = barrasAlto;
            jugador[0].pos.x = barraDistancia;
            jugador[0].pos.y = altoPantalla / 2 - barrasAlto / 2;
            jugador[0].velocidad = velocidad;
            jugador[0].color[0] = SKYBLUE;
            jugador[0].color[1] = MAROON;
            jugador[0].color[2] = GREEN;
            jugador[0].color[3] = ORANGE;
            jugador[0].color[4] = GOLD;
            jugador[0].color[5] = PURPLE;
            jugador[0].color[6] = VIOLET;
            jugador[0].color[7] = BROWN;
            jugador[0].color[8] = MAGENTA;
            jugador[0].color[9] = BLACK;

            // Inicialización del Jugador 2
            jugador[1].radioEsquina = 100;
            jugador[1].pos.width = barrasAncho;
            jugador[1].pos.height = barrasAlto;
            jugador[1].pos.x = anchoPantalla - barraDistancia - barrasAncho * 2;
            jugador[1].pos.y = altoPantalla / 2 - barrasAlto / 2;
            jugador[1].velocidad = velocidad;
            jugador[1].color[0] = SKYBLUE;
            jugador[1].color[1] = MAROON;
            jugador[1].color[2] = GREEN;
            jugador[1].color[3] = ORANGE;
            jugador[1].color[4] = GOLD;
            jugador[1].color[5] = PURPLE;
            jugador[1].color[6] = VIOLET;
            jugador[1].color[7] = BROWN;
            jugador[1].color[8] = MAGENTA;
            jugador[1].color[9] = BLACK;

            // Inicialización de la Pelota
            pelota[0].estado = true;
            pelota[0].centro.x = anchoPantalla / 2;
            pelota[0].centro.y = altoPantalla / 2;
            if (rand() % 2 == 0)
            {
                pelota[0].vel.y = 3;
            }
            else
            {
                pelota[0].vel.y = -3;
            }
            if (rand() % 2 == 0)
            {
                pelota[0].vel.x = 3;
            }
            else
            {
                pelota[0].vel.x = -3;
            }
            pelota[1] = pelota[0];
            pelota[1].estado = false;

            // Escenario
            gameOver01.pos.x = 0;
            gameOver01.pos.y = 0;
            gameOver01.pos.width = jugador[0].pos.x;
            gameOver01.pos.height = altoPantalla;
            gameOver01.color = RED;

            gameOver02.pos.x = jugador[1].pos.x + jugador[1].pos.width;
            gameOver02.pos.y = 0;
            gameOver02.pos.width = anchoPantalla - gameOver02.pos.x - jugador[1].pos.width;
            gameOver02.pos.height = altoPantalla;
            gameOver02.color = RED;

            escenario.enjuego = false;
            escenario.tiempo[0] = clock();
            escenario.tiempo[1] = clock();
            escenario.tiempo[2] = clock();

            // PowerUps
            powerUp.escudo.width = 1;
            powerUp.escudo.height = altoPantalla;
            powerUp.escudo.x = 0;
            powerUp.escudo.y = 0;
        }

        void IniciarBotones()
        {
            menuInmersion = 0;
            menuActual[menuInmersion] = MENUACTUAL::MENUPRINCIPAL;

            int anchoPantalla = GetScreenWidth();
            int altoPantalla = GetScreenHeight();

            boton.mouse.x = 0;
            boton.mouse.y = 0;

            int ancho = 150;
            int alto = 35;

            boton.salir.width = ancho;
            boton.salir.height = alto;
            boton.salir.x = anchoPantalla * 7 / 8 - boton.salir.width / 2;
            boton.salir.y = altoPantalla * 7 / 8 - boton.salir.height / 2;

            // ----------------- Menu Principal -----------------
            boton.menuPrincipalJugar.width = ancho;
            boton.menuPrincipalJugar.height = alto;
            boton.menuPrincipalJugar.x = anchoPantalla * 4 / 8 - boton.menuPrincipalJugar.width / 2;
            boton.menuPrincipalJugar.y = altoPantalla * 2 / 8 - boton.menuPrincipalJugar.height / 2;

            boton.menuPrincipalConfig.width = ancho;
            boton.menuPrincipalConfig.height = alto;
            boton.menuPrincipalConfig.x = boton.menuPrincipalJugar.x;
            boton.menuPrincipalConfig.y = altoPantalla * 4 / 8 - boton.menuPrincipalConfig.height / 2;

            // ------------------ Opciones ---------------------
            boton.volver.width = ancho;
            boton.volver.height = alto;
            boton.volver.x = anchoPantalla * 5 / 8 - boton.volver.width / 2;
            boton.volver.y = altoPantalla * 7 / 8 - boton.volver.height / 2;

            boton.volverMenuPrincipal.width = ancho * 2;
            boton.volverMenuPrincipal.height = alto;
            boton.volverMenuPrincipal.x = anchoPantalla * 6 / 8 - boton.volverMenuPrincipal.width / 2;
            boton.volverMenuPrincipal.y = altoPantalla * 6 / 8 - boton.volverMenuPrincipal.height / 2;

            // -------- Musica --------
            boton.menuOpcionesMusica.width = ancho;
            boton.menuOpcionesMusica.height = alto;
            boton.menuOpcionesMusica.x = anchoPantalla * 1 / 8 - boton.menuOpcionesMusica.width / 2;
            boton.menuOpcionesMusica.y = altoPantalla * 5 / 8 - boton.menuOpcionesMusica.height / 2;

            boton.menuOpcionesMusicaOnOff.width = ancho;
            boton.menuOpcionesMusicaOnOff.height = alto;
            boton.menuOpcionesMusicaOnOff.x = anchoPantalla * 3 / 8 - boton.menuOpcionesMusicaOnOff.width / 2;
            boton.menuOpcionesMusicaOnOff.y = altoPantalla * 5 / 8 - boton.menuOpcionesMusicaOnOff.height / 2;

            // -------- Efectos --------
            boton.menuOpcionesEfectos.width = ancho;
            boton.menuOpcionesEfectos.height = alto;
            boton.menuOpcionesEfectos.x = anchoPantalla * 1 / 8 - boton.menuOpcionesEfectos.width / 2;
            boton.menuOpcionesEfectos.y = altoPantalla * 7 / 8 - boton.menuOpcionesEfectos.height / 2;

            boton.menuOpcionesEfectosOnOff.width = ancho;
            boton.menuOpcionesEfectosOnOff.height = alto;
            boton.menuOpcionesEfectosOnOff.x = anchoPantalla * 3 / 8 - boton.menuOpcionesEfectosOnOff.width / 2;
            boton.menuOpcionesEfectosOnOff.y = altoPantalla * 7 / 8 - boton.menuOpcionesEfectosOnOff.height / 2;

            // -------- Controles Configurables -------- (Jugador 1)
            boton.p1Arriba.width = ancho;
            boton.p1Arriba.height = alto;
            boton.p1Arriba.x = anchoPantalla * 1 / 8 - boton.p1Arriba.width / 2;
            boton.p1Arriba.y = altoPantalla * 2 / 8 - boton.p1Arriba.height / 2;

            boton.p1ArribaInteractivo.width = ancho;
            boton.p1ArribaInteractivo.height = alto;
            boton.p1ArribaInteractivo.x = anchoPantalla * 3 / 8 - boton.p1ArribaInteractivo.width / 2;
            boton.p1ArribaInteractivo.y = altoPantalla * 2 / 8 - boton.p1ArribaInteractivo.height / 2;

            boton.p1Abajo.width = ancho;
            boton.p1Abajo.height = alto;
            boton.p1Abajo.x = anchoPantalla * 1 / 8 - boton.p1Abajo.width / 2;
            boton.p1Abajo.y = altoPantalla * 3 / 8 - boton.p1Abajo.height / 2;

            boton.p1AbajoInteractivo.width = ancho;
            boton.p1AbajoInteractivo.height = alto;
            boton.p1AbajoInteractivo.x = anchoPantalla * 3 / 8 - boton.p1AbajoInteractivo.width / 2;
            boton.p1AbajoInteractivo.y = altoPantalla * 3 / 8 - boton.p1AbajoInteractivo.height / 2;
            // -------- Controles Configurables -------- (Jugador 2)
            boton.p2Arriba.width = ancho;
            boton.p2Arriba.height = alto;
            boton.p2Arriba.x = anchoPantalla * 5 / 8 - boton.p2Arriba.width / 2;
            boton.p2Arriba.y = altoPantalla * 2 / 8 - boton.p2Arriba.height / 2;

            boton.p2ArribaInteractivo.width = ancho;
            boton.p2ArribaInteractivo.height = alto;
            boton.p2ArribaInteractivo.x = anchoPantalla * 7 / 8 - boton.p2ArribaInteractivo.width / 2;
            boton.p2ArribaInteractivo.y = altoPantalla * 2 / 8 - boton.p2ArribaInteractivo.height / 2;

            boton.p2Abajo.width = ancho;
            boton.p2Abajo.height = alto;
            boton.p2Abajo.x = anchoPantalla * 5 / 8 - boton.p2Abajo.width / 2;
            boton.p2Abajo.y = altoPantalla * 3 / 8 - boton.p2Abajo.height / 2;

            boton.p2AbajoInteractivo.width = ancho;
            boton.p2AbajoInteractivo.height = alto;
            boton.p2AbajoInteractivo.x = anchoPantalla * 7 / 8 - boton.p2AbajoInteractivo.width / 2;
            boton.p2AbajoInteractivo.y = altoPantalla * 3 / 8 - boton.p2AbajoInteractivo.height / 2;

            boton.pausa.width = ancho;
            boton.pausa.height = alto;
            boton.pausa.x = anchoPantalla * 5 / 8 - boton.pausa.width / 2;
            boton.pausa.y = altoPantalla * 4 / 8 - boton.pausa.height / 2;

            boton.pausaInteractivo.width = ancho;
            boton.pausaInteractivo.height = alto;
            boton.pausaInteractivo.x = anchoPantalla * 7 / 8 - boton.pausaInteractivo.width / 2;
            boton.pausaInteractivo.y = altoPantalla * 4 / 8 - boton.pausaInteractivo.height / 2;

            // ---------------- Menu GameOver -----------------
            boton.menuGameOverReiniciar.width = ancho;
            boton.menuGameOverReiniciar.height = alto;
            boton.menuGameOverReiniciar.x = anchoPantalla * 4 / 8 - boton.menuGameOverReiniciar.width / 2;
            boton.menuGameOverReiniciar.y = altoPantalla * 2 / 8 - boton.menuGameOverReiniciar.height / 2;

            boton.menuGameOverVolverMenu.width = ancho;
            boton.menuGameOverVolverMenu.height = alto;
            boton.menuGameOverVolverMenu.x = boton.menuGameOverReiniciar.x;
            boton.menuGameOverVolverMenu.y = altoPantalla * 4 / 8 - boton.menuGameOverVolverMenu.height / 2;
        }
        
        void ReiniciarPuntaje()
        {
            jugador[0].puntaje = 0;
            jugador[1].puntaje = 0;
        }
    }

}