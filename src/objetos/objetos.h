#ifndef OBJETOS_H
#define OBJETOS_H

#include <time.h>
#include <stdlib.h>

#include "raylib.h"

#include "gameplay/power_up.h"
#include "objetos/objetos.h"
#include "menu/menues.h"
#include "gameplay/evento_random.h"

using namespace pong::powerups;

namespace pong
{
    namespace objetos
    {
        const int jugadoresTotales = 2;
        const int pelotasTotales = 2;
        struct JUGADOR
        {
            Rectangle pos;

            int puntaje = 0;
            int radioEsquina;
            int velocidad;
            bool estadoIA = false;

            Texture2D textura;
            int colorActual = 0;
            Color color[10];
        };
        struct PELOTA
        {
            bool estado;
            Vector2 centro;
            int radio = 20;
            bool aumentoY;

            Vector2 vel;
            Texture2D textura;
            Color color = VIOLET;
        };
        struct GAMEOVER
        {
            Rectangle pos;
            Color color;
        };
        struct BOTONES
        {
            // General
            bool pulsador;
            Vector2 mouse;
            Rectangle salir;
            Color musicaColor;
            Color efectoColor;

            // Menu Principal
            Rectangle menuPrincipalJugar;
            Rectangle menuPrincipalConfig;
            Rectangle menuPrincipalCreditos;

            // Opciones
            Rectangle menuOpcionesMusica;
            Rectangle menuOpcionesMusicaOnOff;
            Rectangle menuOpcionesEfectos;
            Rectangle menuOpcionesEfectosOnOff;
            Rectangle volver;
            Rectangle volverMenuPrincipal;

            Rectangle p1Arriba;
            Rectangle p1ArribaInteractivo;
            Rectangle p1Abajo;
            Rectangle p1AbajoInteractivo;

            Rectangle p2Arriba;
            Rectangle p2ArribaInteractivo;
            Rectangle p2Abajo;
            Rectangle p2AbajoInteractivo;

            Rectangle pausa;
            Rectangle pausaInteractivo;

            // Game Over
            Rectangle menuGameOverReiniciar;
            Rectangle menuGameOverVolverMenu;
        };
        struct ESCENARIO
        {
            int margenSuperior = 0;
            bool enjuego;
            int tiempo[3];
            int puntuacionMax = 3;
        };

        extern JUGADOR jugador[jugadoresTotales];
        extern PELOTA pelota[pelotasTotales];
        extern GAMEOVER gameOver01;
        extern GAMEOVER gameOver02;

        extern BOTONES boton;
        extern ESCENARIO escenario;

        void IniciarObjetos();
        void IniciarBotones();
        void ReiniciarPuntaje();
    }
}

#endif