#ifndef RESOURCES_H
#define RESOURCES_H

#include "raylib.h"

#include "menu/menues.h"

namespace pong
{
	namespace configuraciones
	{
		extern bool musica;
		extern bool efectos;
	}

	/*namespace sonidos
	{
		const char rutaMusicaMenu[] = "musicaMenu.mp3";
		const char rutaMusicaJuego[] = "musicaJuego.mp3";
		const char rutaEfectoRebote[] = "efectoRebote.mp3";
		const char rutaEfectoGameOver[] = "efectoGameOver.mp3";
		extern Music musicaMenu;
		extern Music musicaJuego;
		extern Music efectoRebote;
		extern Music efectoGameOver;
		void init();
		void update();
		void deinit();
	}*/
}

#endif